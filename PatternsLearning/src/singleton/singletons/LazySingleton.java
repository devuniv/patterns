package singleton.singletons;

//initialization-on-demand holder idiom
//lazy thread-safe singleton
public final class LazySingleton {

	//1) code in static initializer for nested class is executed only
	//when INSTANCE static variable first used,
	//that is when LazySingleton.getInstance() first called
	//2) Initialization of INSTANCE is guaranteed thread-safe
	//so multiple concurrent calling getInstance() 
	//all times return correctly initialized same INSTANCE 
	private static class SingletonHolder {
		private static final LazySingleton INSTANCE = new LazySingleton();	
		
	}
	
	private LazySingleton() {
		
	}
	
	//private member of private nested class is directly accessible
	//in outer class method!
	public static LazySingleton getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public void f() {
		System.out.println("LazySingleton with SingletonHolder working!");
	}
}
