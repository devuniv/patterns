package singleton.singletons;


//better use
//initialization-on-demand holder idiom
public final class SyncLazySingleton {

	private static SyncLazySingleton instance = null;

	public static synchronized SyncLazySingleton getInstance() {
		if (instance == null) {
			instance = new SyncLazySingleton();
		}

		return instance;
	}
	
	private SyncLazySingleton() {
		
	}
	
	public void f() {
		System.out.println("SyncLazySingleton working!");
	}

}
