package singleton.singletons;

//eager singleton example

//clas is final b/c constr is private
public final class EagerSingleton {	
	
	
	private static final EagerSingleton INSTANCE = new EagerSingleton();
	
	//prevents creating object with new
	private EagerSingleton() { /* useful code goes here */ }
	
	public static EagerSingleton getInstance() {
		return INSTANCE;
	}
	
	
		public void f() {
			System.out.println("EagerSingleton working!");
		}
	
	

}
