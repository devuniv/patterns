package singleton.driver;

import singleton.singletons.EagerSingleton;
import singleton.singletons.LazySingleton;
import singleton.singletons.SyncLazySingleton;

public class Driver {

	public static void main(String[] args) {

		EagerSingleton sg = EagerSingleton.getInstance();
		sg.f();
		
		LazySingleton lsg = LazySingleton.getInstance();
		lsg.f();
		
		SyncLazySingleton slsg = SyncLazySingleton.getInstance();
		slsg.f();
		
		
	}

}
