package singleton.driver;

public final class WrongSingleton {

	private static class SingletonHolder {
		//write = new... here
		//lazy is guaranteed by nested class loading!
		private static WrongSingleton instance = null;

		// 1. private method would also be called OK
		// from WrongSingleton.getInstance() !
		public static WrongSingleton getInstance() {
			if (instance == null) {
				instance = new WrongSingleton();
			}
			return instance;
		}
	}

}
