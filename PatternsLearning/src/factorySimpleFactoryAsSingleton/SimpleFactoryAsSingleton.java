package factorySimpleFactoryAsSingleton;

import factorySimpleFactoryAsSingleton.AnimalFactory.AnimalType;

//interface Animal could be abstract class Animal, Dog extends Animal

interface Animal {
	void makeSound();
}

class Dog implements Animal {
	@Override
	public void makeSound() {
		System.out.println("Woof...");
	}
}

class Cat implements Animal {
	@Override
	public void makeSound() {
		System.out.println("Meow...");
	}
}

// factory class name = interfaceName + "Factory"
// alternative: AnimalSimpleFactory (it's BAD - later you may want refactor
// simple factory to Factory Method, so better call AnimalFactory w/o Simple)

// initialization-on-demand holder idiom
// lazy thread-safe singleton
final class AnimalFactory {

	enum AnimalType {
		CAT, DOG;
	}

	// singleton holder class
	private static class AnimalFactoryHolder {
		private static final AnimalFactory INSTANCE = new AnimalFactory();
	}

	private AnimalFactory() {
	}

	public static AnimalFactory getInstance() {
		return AnimalFactoryHolder.INSTANCE;
	}

	// Factory Method
	// alternative name: getAnimal - for Animal interface
	// or w/o enum: Product createProduct(ProductID)
	// non-static for easy mocking and thus testing
	// using singleton for stateless class with static methods is silly
	public Animal create(AnimalType animalType) {
		switch (animalType) {
		case CAT:
			// Cat cat = new Cat();
			// cat.setUpSth(anotherArgOfCreateMethod);
			// return cat; // with setter methods already called
			return new Cat();
		case DOG:
			return new Dog();

		default:
			// treat null animalType argument!
			String msg = "type argument denoting object to create cannot be null";
			throw new IllegalArgumentException(msg);
		}

		// you need dummy return here if don't throw exception in default:
		// branch
		// LOG that control got somewhere, you never expected
		// String logMsg = "control shall never get here";
		// return null;
	}
}

class Driver {
	public static void main(String[] args) {
		AnimalFactory animalFactory = AnimalFactory.getInstance();
		Animal animal = animalFactory.create(AnimalType.DOG);
		animal.makeSound();
	}
}
