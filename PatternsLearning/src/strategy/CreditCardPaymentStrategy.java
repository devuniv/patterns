package strategy;

//concrete Strategy class 1 (implements abstract Strategy interface)
public class CreditCardPaymentStrategy implements PaymentStrategy {

	private String name;
	private String cardNumber;
	private String cvv;
	private String dateOfExpiry;

	public CreditCardPaymentStrategy(String nm, String ccNum, String cvv,
			String expiryDate) {
		this.name = nm;
		this.cardNumber = ccNum;
		this.cvv = cvv;
		this.dateOfExpiry = expiryDate;
	}

	@Override
	public void pay(int amount) {
		// TODO make actual payment...
		System.out.println(amount + " paid with credit/debit card");
	}

}