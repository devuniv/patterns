package strategy;

//this interface will be used by Context class 
//(temporarily passed as arg to some of its methods to be used locally 
//within that method - without being stored as field)

//this interface will be implemented by concrete classes,
//specifying different payment algorithms

public interface PaymentStrategy {
	public void pay(int amount);
}
