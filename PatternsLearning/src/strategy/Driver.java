package strategy;

//read more
//http://www.journaldev.com/1754/strategy-design-pattern-in-java-example-tutorial

public class Driver {

	public static void main(String[] args) {
		ShoppingCart cart = new ShoppingCart();

		// optional arg:
		// new PaypalPaymentStrategy("myemail@example.com", "mypwd")
		cart.pay(new CreditCardPaymentStrategy("Pankaj Kumar",
				"1234567890123456", "786", "12/15"));
	}

}
