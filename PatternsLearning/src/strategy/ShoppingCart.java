package strategy;

// for nice full shopping cart see

// http://www.journaldev.com/1754/strategy-design-pattern-in-java-example-tutorial

// Context class  (but often "Context class" is a quite different thing!!!)
// context - because in this class (= in this context) strategy is used
// alternative name: SthPaymentContext - method pay may be called from
// different classes, each of them being specific PaymentContext
// alternative name: ShoppingCartPaymentContext
public class ShoppingCart {

	public void pay(PaymentStrategy paymentMethod) {
		int amount = 100; // sure, shall not be hard-coded like this

		// using different PaymentStrategy algorithm in RT
		paymentMethod.pay(amount);
	}
}
