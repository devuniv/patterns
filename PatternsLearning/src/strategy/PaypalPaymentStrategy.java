package strategy;

public class PaypalPaymentStrategy implements PaymentStrategy {

	private String emailId;
	private String password;

	public PaypalPaymentStrategy(String email, String pwd) {
		this.emailId = email;
		this.password = pwd;
	}

	// concrete Strategy class 2 (implements abstract Strategy interface)
	@Override
	public void pay(int amount) {
		// TODO make actual payment...
		System.out.println(amount + " paid using Paypal.");
	}

}