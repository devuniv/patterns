package adapter.client_example2;

import adapter.client_example2.library_for_client.Airplane;
import adapter.client_example2.library_for_client.Car;
import adapter.client_example2.library_for_client.LibraryUsedByClient;
import adapter.client_example2.library_for_client.Vehicle;
import adapter.somelibrary.Spaceship;

//type: Object Adapter (composition) - preferable 
//Adaptee - class Spaceship
//Target -  Vehicle interface
class SpaceshipToVehicleAdapter implements Vehicle {
	private Spaceship spaceship = new Spaceship();

	@Override
	public void go() {
		spaceship.start(30, 30);
	}

	@Override
	public void stop() {
		spaceship.land(30, 30);
	}
	
	
}

public class ClientExample2 {

	public static void main(String[] args) {

		LibraryUsedByClient obj = new LibraryUsedByClient();

		obj.usefulMethod1(new Car());
		obj.usefulMethod2(new Airplane());
		
		//use lib method taking Vehicle as parameter
//		with Spaceship object which doesn't impl such iface
		obj.usefulMethod1(new SpaceshipToVehicleAdapter());

		// now want to call lib func which requires
		// object implementing Vehicle interface
		// Spaceship suits it logically, but doesn't
		// implement this interface and its methods
		// (which are doing the same) are calledd differently
		// than interface methods

	}

}
