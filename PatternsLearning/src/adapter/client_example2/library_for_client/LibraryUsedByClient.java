package adapter.client_example2.library_for_client;

public class LibraryUsedByClient {
	
	public void usefulMethod1(Vehicle vehicle) {
		System.out.println("LibraryUsedByClient.usefulMethod1");
	}
	
	public void usefulMethod2(Vehicle vehicle) {
		System.out.println("LibraryUsedByClient.usefulMethod2");
	}

}
