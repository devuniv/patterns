package adapter.client_example2.library_for_client;

public class Airplane implements Vehicle {

	@Override
	public void go() {
		System.out.println("Airplane starting!");

	}

	@Override
	public void stop() {
		System.out.println("Airplane stopping!");

	}

}