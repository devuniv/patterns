package adapter.client_example2.library_for_client;

public class Car implements Vehicle {

	@Override
	public void go() {
		System.out.println("Car starting!");
	}

	@Override
	public void stop() {
		System.out.println("Car stopping!");

	}

}