package adapter.client_example2.library_for_client;

public interface Vehicle {
	void go();

	void stop();
}
