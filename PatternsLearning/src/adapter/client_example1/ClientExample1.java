package adapter.client_example1;

import java.util.ArrayList;
import java.util.List;

import adapter.somelibrary.Spaceship;

//Client's programmer programs to this interface
//using only interface name in method calls
interface Vehicle {
	void go();

	void stop();
}

class Car implements Vehicle {

	@Override
	public void go() {
		System.out.println("Car starting!");
	}

	@Override
	public void stop() {
		System.out.println("Car stopping!");

	}

}

class Airplane implements Vehicle {

	@Override
	public void go() {
		System.out.println("Airplane starting!");

	}

	@Override
	public void stop() {
		System.out.println("Airplane stopping!");

	}

}

// we want use Spaceship in Client (ClientExample1)
//
// Adapter type: Class Adapter (inheritance of Spaceship)
// Naming convention for Adapter - TargetAdapter
//
// Target - Vehicle interface
// Target methods - methods of Vehicle interface
// Adaptee - Spaceship class
// spelling: adapter = adaptor. Both correct. Adapter - more often, both in US &
// GB
class VehicleAdapter extends Spaceship implements Vehicle {

	@Override
	public void go() {
		//Spaceship method, same as go() but with different name
		start(30, 30);
	}

	@Override
	public void stop() {
		land(30, 30);
	}

}

public class ClientExample1 {

	public void appLogic() {

		List<Vehicle> lst = new ArrayList<>();
		lst.add(new Car());
		lst.add(new Airplane());

		// now I want add Spaceship to the list
		// Spaceship methods do the same logic, but they are named differently
		// so write adapter class and use it
		lst.add(new VehicleAdapter());

		for (Vehicle vehicle : lst) {
			vehicle.go();
			vehicle.stop();
		}

	}

	public static void main(String[] args) {
		new ClientExample1().appLogic();
	}

}
