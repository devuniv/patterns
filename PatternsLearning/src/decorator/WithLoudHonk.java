package decorator;

//concrete Decorator 1.
//Alternative Name: LoudHonkingCar, LoudHonkFeaturedCar, LoudHonkCar,
//WithLoudHonkDecorator, LoudHonkingCarDecorator, LoudHonkCarDecorator
//(thou "Decorator"  word is superfluous - "extends Decorator" explains it!)
// Usually adds one feature (LoudHonk) which is described by its class name
// this feature may mean overriding one OR MORE methods of Car interface
public class WithLoudHonk extends CarDecorator {

	public WithLoudHonk(Car carToBeDecorated) {
		super(carToBeDecorated);
	}

	// Additional functionality can be harcoded into this method.
	// Much better approach is to use private method for this
	// additional functionality.
	@Override
	public void drive() {
		// also can add functionality BEFORE BasicCar functionality
		// System.out.println("honking loudly BEFORE started to move");
		makeHonkingSoundBeforeDrive();
		super.drive();
		makeHonkingSoundAfterDrive();
		// adding functionality AFTER BasicCar functionality
		// System.out.println("honking loudly AFTER started to move");
	}

	// method contains additional functionality
	private void makeHonkingSoundBeforeDrive() {
		System.out.println("honking loudly BEFORE started to move");
	}

	// method contains additional functionality
	private void makeHonkingSoundAfterDrive() {
		System.out.println("honking loudly AFTER started to move");
	}

	// this class does not add anything to stop() method, but it could!

}