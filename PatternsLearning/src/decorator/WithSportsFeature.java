package decorator;

//concrete Decorator 2.
//Alternative Name: SportsFeaturedCar, SportsCar,
//WithSportsFeatureDecorator, SportsCarDecorator
public class WithSportsFeature extends CarDecorator {

	public WithSportsFeature(Car carToBeDecorated) {
		super(carToBeDecorated);
	}

	@Override
	public void stop() {
		makeScreechingSound();
		super.stop();
	}

	private void makeScreechingSound() {
		System.out.println("superfast sports car's wheels screeching to stop!");
	}

	// this class does not add anything to drive() method, but it could!

}