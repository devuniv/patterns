package decorator;

//Basic Component (implements component iface, provides basic functionality
//additional functionality will be added by concrete decorator classes).
//Alternative Names: SimpleCar, BasicComponent, MainComponent
public class BasicCar implements Car {

	@Override
	public void drive() {
		System.out.println("Basic Car moving/rolling!");
	}

	@Override
	public void stop() {
		// NOP
		// real Car surely must implement stop() :))
	}

}