package decorator;

public class Driver {

	public static void main(String[] args) {

		// no Decorator so far - just using class BasicCar as ususally.
		Car mySimpleCar = new BasicCar();
		mySimpleCar.drive();
		mySimpleCar.stop();

		System.out.println("***********************");

		// Now using Decorator pattern.
		// Some people use abstract Decorator class instead Component iface:
		// CarDecorator myCarWithAddedFeatures;
		Car myCarWithAddedFeatures;
		myCarWithAddedFeatures = new WithSportsFeature(
				new WithLoudHonk(mySimpleCar));

		myCarWithAddedFeatures.drive();
		myCarWithAddedFeatures.stop();

		// some construct Decorated car not as one-liner, but step-by-step:
		Car myCarWithLoudHonk = new WithLoudHonk(mySimpleCar);
		Car myCarWithLoudHonkAndSportsFeature = new WithSportsFeature(
				myCarWithLoudHonk);

		System.out.println("***********************");

		myCarWithLoudHonkAndSportsFeature.drive();
		myCarWithLoudHonkAndSportsFeature.stop();
	}

}

// https://en.wikipedia.org/wiki/Decorator_pattern#Java

// http://www.journaldev.com/1540/decorator-design-pattern-in-java-example

// https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D0%BA%D0%BE%D1%80%D0%B0%D1%82%D0%BE%D1%80_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)