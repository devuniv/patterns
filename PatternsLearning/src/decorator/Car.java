package decorator;

//Component interface
public interface Car {

	void drive();

	void stop();
}
