package decorator;

//(abstract) Decorator class. 
//Adds nothing to interface method.
//will be used as template for all concrete decorator classes
//which would 
//1. call ctor of this abstract class
//2. methods overriding drive would call super.drive() + add their functionality
//before or after calling super.drive() - thus modifying.
public abstract class CarDecorator implements Car {
	protected final Car carToDecorate; // carBeforeThisDecorator

	public CarDecorator(Car car) {
		this.carToDecorate = car;
	}

	@Override
	public void drive() {
		carToDecorate.drive();
	}

	@Override
	public void stop() {
		carToDecorate.stop();
	}

}