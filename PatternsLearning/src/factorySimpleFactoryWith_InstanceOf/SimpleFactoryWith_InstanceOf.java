package factorySimpleFactoryWith_InstanceOf;

import java.io.File;

import org.w3c.dom.Document;

//Abstract Product
//might be interface
abstract class AbstractWriter {
	public abstract void write(Object context);
}

// Concrete Product
class ConcreteFileWriter extends AbstractWriter {
	@Override
	public void write(Object context) {
		// method body
	}
}

// Concrete Product
class ConcreteXmlWriter extends AbstractWriter {
	@Override
	public void write(Object context) {
		// method body
	}
}

class WriterFactory {

	public static AbstractWriter getWriter(Object object) {
		AbstractWriter writer = null;
		if (object instanceof File) {
			writer = new ConcreteFileWriter();
		} else if (object instanceof Document) {
			writer = new ConcreteXmlWriter();
		}
		return writer;
	}
}

class Driver {
	File file = new File("c:/tmp/file.txt");
	// we already have file Object to work with, maybe we got it from some other
	// part of program
	AbstractWriter writer = WriterFactory.getWriter(file); // instantiates with
															// ConcreteFileWriter
}