package factorySimpleFactory;

import factorySimpleFactory.AnimalFactory.AnimalType;

//interface Animal could be abstract class Animal, Dog extends Animal

interface Animal {
	void makeSound();
}

class Dog implements Animal {
	@Override
	public void makeSound() {
		System.out.println("Woof...");
	}
}

class Cat implements Animal {
	@Override
	public void makeSound() {
		System.out.println("Meow...");
	}
}

// factory class name = interfaceName + "Factory"
// alternative: AnimalSimpleFactory (it's BAD - later you may want refactor
// simple factory to Factory Method, so better call AnimalFactory w/o Simple)

// Simple Factory for interface Animal
// often this is singleton, no need for many factories
class AnimalFactory {

	enum AnimalType {
		CAT, DOG;
	}

	// alternative name: getAnimal - for Animal interface
	// or w/o enum: Product createProduct(ProductID)
	public static Animal create(AnimalType animalType) {

		switch (animalType) {
		case CAT:
			// Cat cat = new Cat();
			// cat.setUpSth(anotherArgOfCreateMethod);
			// return cat; // with setter methods already called
			return new Cat();
		case DOG:
			return new Dog();

		default:
			// treat null animalType argument!
			String msg = "type argument denoting object to create cannot be null";
			throw new IllegalArgumentException(msg);
		}

		// you need dummy return here if don't throw exception in default:
		// branch
		// LOG that control got somewhere, you never expected
		// String logMsg = "control shall never get here";
		// return null;
	}

	// STRING ARG IMPLEMENTATION - WORST TO USE
	// public Animal getAnimal(String animal) {
	// if (animal.isEmpty())
	// return null;
	//
	// if (animal.equalsIgnoreCase("Dog"))
	// return new Dog();
	// else if (animal.equalsIgnoreCase("Cat"))
	// return new Cat();
	// else
	// return null;
	// }
}

class Driver {
	public static void main(String[] args) {
		// Animal animal = AnimalFactory.create(AnimalFactory.AnimalType.CAT);
		Animal animal = AnimalFactory.create(AnimalType.CAT);
		// import static pkgname.AnimalFactory.AnimalType;
		animal.makeSound();

		// STRING ARG IMPLEMENTATION - WORST TO USE
		// Animal animal = AnimalFactory.create("Dog");
	}
}

// http://www.javenue.info/post/17
//