package bridge;


/**
 * Bridge interface. First part of varying logic (color) - see classes implementing this interface
 *
 */
interface Color {
  void fill(); // this part #1 of logic would vary independently from part #2
}


abstract class Shape {

  protected Color color;

  protected Shape(Color color) {
    this.color = color;
  }

  // this part #2 of logic would vary independently from part #1
  // thou it would use part #1 logic in its method, combibibg it w/ its own logic
  abstract void draw();
}


class Red implements Color {
  @Override
  public void fill() {
    System.out.println("paint in red");
  }
}


class Blue implements Color {
  @Override
  public void fill() {
    System.out.println("paint in blue");
  }
}


class Square extends Shape {

  protected Square(Color color) {
    super(color);
  }

  @Override
  void draw() {
    System.out.print("draw Square + "); // part of varying logic from abstract cls hierarhy
    color.fill(); // add varying logic from misc interface implementations
  }
}


class Circle extends Shape {

  protected Circle(Color color) {
    super(color);
  }

  @Override
  void draw() {
    System.out.print("draw Circle + "); // part of varying logic from abstract cls hierarhy
    color.fill(); // add varying logic from misc interface implementations
   
  }
}



public class Bridge {

  public static void main(String[] args) {

    Shape shape1 = new Square(new Red());
    Shape shape2 = new Circle(new Blue());

    shape1.draw();
    shape2.draw();
  }

}
