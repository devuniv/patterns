package observer_java_api.driver;

import observer_java_api.observables.MyObservable;
import observer_java_api.observables.MyObservable1;
import observer_java_api.observers.MyObserver;
import observer_java_api.observers.MyObserver1;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MyObserver myObserver = new MyObserver();
		MyObservable myObservable = new MyObservable(myObserver);
		myObservable.addObserver(myObserver);
		myObservable.addObserver(myObserver);
		myObservable.addObserver(myObserver);
		myObservable.setSpeed(89);
		
		MyObserver1 myObserver1 = new MyObserver1();
		MyObservable1 myObservable1 = new MyObservable1();
		myObservable1.addObserver(myObserver1);
		myObservable1.setTemperature(100);
		
		

	}

}
