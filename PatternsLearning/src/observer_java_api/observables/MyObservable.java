package observer_java_api.observables;

import java.util.Observable;
import java.util.Observer;



public class MyObservable extends Observable {

	public MyObservable(Observer observer) {
		super.addObserver(observer);

	}

	public void addObserver(Observer observer) {
		super.addObserver(observer);
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		System.out.println("notify called");
		super.notifyObservers();
		
		//if you want to pass sth to Observer, use
		//void notifyObservers(Object arg) from Observable class
	}

	private int speed = 0;



	public void setSpeed(int speed) {
		this.speed = speed;
		setChanged();
		notifyObservers();
//		setChanged();
	}

}
