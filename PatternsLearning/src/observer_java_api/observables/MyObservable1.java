package observer_java_api.observables;

import java.util.Observable;
import java.util.Observer;

public class MyObservable1 {

	// we need to call
	// protected Observable.setChanged()
	private static class EnhancedObservable extends Observable {

		public void setChanged() {
			super.setChanged();
		}
	}

	private EnhancedObservable observableSupport = new EnhancedObservable();

	private int temperature;

	public void setTemperature(int temperature) {
		this.temperature = temperature;
		observableSupport.setChanged();
		observableSupport.notifyObservers(temperature);

	}

	public void addObserver(Observer observer) {
		observableSupport.addObserver(observer);
	}

	public void notifyObservers() {
		observableSupport.notifyObservers();
	}

	public void notifyObservers(Object dataPassToObserver) {
		observableSupport.notifyObservers(dataPassToObserver);
	}

	// testing only
	public static void main(String[] args) {

	}

}
