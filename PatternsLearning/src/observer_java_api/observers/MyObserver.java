package observer_java_api.observers;

import java.awt.Frame;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

public class MyObserver implements Observer {

	// what this object does when it learns that Observable changed
	// and got Object arg from Observable (can use it in what it does)
	@Override
	public void update(Observable o, Object arg) {
System.out.println("updated");
		JOptionPane msgBox = new JOptionPane();
		msgBox.showInputDialog("I am notified");
		//msgBox.showInputDialog("Observable says: " + arg);
		
	}

	
	
	//test only
	public static void main(String[] args) {
		
		new MyObserver().update(null, "werewr");
	}

}
