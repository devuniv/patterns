package observer_java_api.observers;

import java.util.Observable;
import java.util.Observer;

public class MyObserver1 implements Observer {

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("I see change in class: " + o.getClass().getName());
		System.out.println("temperature changed! Now t = " + (int) arg);
		
	}

	
	
}
