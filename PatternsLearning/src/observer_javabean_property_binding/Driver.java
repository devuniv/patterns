package observer_javabean_property_binding;

public class Driver {

	public static void main(String[] args) {
EventSourceBean eventSource = new EventSourceBean();
EventListenerBean myListener = new EventListenerBean();

eventSource.addPropertyChangeListener(myListener);
eventSource.setTemperature(777);


	}

}
