package observer_javabean_property_binding;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class EventListenerBean implements PropertyChangeListener {

private int temperatureMonitoringResult;

public int getTemperatureMonitoringResult() {
	return temperatureMonitoringResult;
}

public void setTemperatureMonitoringResult(int temperatureMonitoringResult) {
	this.temperatureMonitoringResult = temperatureMonitoringResult;
}

@Override
public void propertyChange(PropertyChangeEvent evt) {
	System.out.println("EventListenerBean knows about change in EventSourceBean");
	System.out.println("old value was: " + evt.getOldValue());
	System.out.println("new value is: " + evt.getNewValue());
	System.out.println("EventSourceBean changed property name is: " + evt.getPropertyName());
	System.out.println("object where property changed: " + evt.getSource().getClass());

}





}
