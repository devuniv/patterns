/*OBSERVER EXAMPLE 
* FOR JAVA BEANS
* USING java.beans.PropertyChangeSupport, PropertyChangeListener, PropertyChangeEvent
* 
* Also called: Property Binding idiom
*/

package observer_javabean_property_binding;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

//only composition of PropertyChangeSupport into your SrcBean is possible
//b/c PropertyChangeSupport has only constructor with argument 
//and you can't write "super(this)" in your SrcBean constructor
//so class EventSourceBean extends PropertyChangeSupport is NOT AN OPTION!
public class EventSourceBean {
	
	//works OK without final, but final is recommended
	private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	private int temperature;

	public int getTemperature() {
		return temperature;
	}

	// if you extend PropertyChangeSupport, call firePropertyChange just that,
	// without propertyChangeSupport. part
	//remember to store oldValue - it is needed for method
	//call firePropertyChange after you set new value of field in setter
	public void setTemperature(int temperature) {
		int oldValue = this.temperature;
		this.temperature = temperature;
		propertyChangeSupport.firePropertyChange("temperature", oldValue, temperature);
	}

	public void addPropertyChangeListener(PropertyChangeListener aListener) {
		propertyChangeSupport.addPropertyChangeListener(aListener);
	}

	public void removePropertyChangeListener(PropertyChangeListener aListener) {
		propertyChangeSupport.removePropertyChangeListener(aListener);
	}

}
