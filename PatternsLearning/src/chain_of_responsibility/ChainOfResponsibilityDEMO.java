package chain_of_responsibility;


abstract class Handler {
	
	// next handler in chain of handlers
	protected Handler next;

	// Derived classes set this property in their constructor.
	// Used to decide what handler class will do the job
	protected int msgImportance;

	// used to construct handler chain of any length
	public void setNext(Handler next) {
		this.next = next;
	}

	// Client code calls only this method (requesting to do the job).
	// Depending on c, this method decides
	// this class handles the request, or we forward handling to next Handler in
	// chain
	public void handleMessage(String msg, int msgPriority) {
		// if this handler class fits criteria (msgImportance) - let it do the job!
		if (msgPriority <= msgImportance) {
			doRealMessageHandling(msg);
			return; // comment out to call subsequent handlers anyway (for other processing needs)
		}

		// proceed to next handler in the chain
		if (next != null) {
			next.handleMessage(msg, msgPriority);
		}
	}

	protected abstract void doRealMessageHandling(String msg);

}

class SpamMessageHandler extends Handler {

	public SpamMessageHandler(int msgImportance) {
		this.msgImportance = msgImportance;
	}

	@Override
	protected void doRealMessageHandling(String msg) {
		System.out.println("processed SPAM successfully!" + " Msg is: " + msg);
	}

}

class RegularMessageHandler extends Handler {

	public RegularMessageHandler(int msgImportance) {
		this.msgImportance = msgImportance;
	}

	@Override
	protected void doRealMessageHandling(String msg) {
		System.out.println("processed RegularMessage successfully!" + " Msg is: " + msg);
	}

}

class ImportantMessageHandler extends Handler {

	public ImportantMessageHandler(int msgImportance) {
		this.msgImportance = msgImportance;
	}

	@Override
	protected void doRealMessageHandling(String msg) {
		System.out.println("processed ImportantMessage successfully!" + " Msg is: " + msg);
	}

}

class LoveMessageHandler extends Handler {

	public LoveMessageHandler(int msgImportance) {
		this.msgImportance = msgImportance;
	}

	@Override
	protected void doRealMessageHandling(String msg) {
		System.out.println("processed LoveMessage successfully!" + " Msg is: " + msg);
	}

}

public class ChainOfResponsibilityDEMO {

	public static void main(String[] args) {

		// instantiate handlers and specify mask.
		// Based on mask handler decides to handle task
		// or to delegate handling to next handler in chain
		Handler myHandler, handler2, handler3, handler4;
		myHandler = new SpamMessageHandler(10);
		handler2 = new RegularMessageHandler(100);
		handler3 = new ImportantMessageHandler(777);
		handler4 = new LoveMessageHandler(999);

		// hook up handler chain
		myHandler.setNext(handler2); // first handler in chain - call this one!
		handler2.setNext(handler3);
		handler3.setNext(handler4);

		// ask chain (thru Handler interface) to do the job 
		myHandler.handleMessage("Some argument", 700); // ImportantMessage handler fits
		myHandler.handleMessage("Some argument", 1); // SpamMessage handler fits
		myHandler.handleMessage("Some argument", 900); // LoveMessage handler fits

	}
}
