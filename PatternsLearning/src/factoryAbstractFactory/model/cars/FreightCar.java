package factoryAbstractFactory.model.cars;

//ConcreteProduct 
public class FreightCar implements Car {

	@Override
	public void start() {
		System.out.println("FreightCar started");
	}
}