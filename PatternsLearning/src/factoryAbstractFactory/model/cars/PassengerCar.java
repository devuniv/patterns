package factoryAbstractFactory.model.cars;

//ConcreteProduct 
public class PassengerCar implements Car {

	@Override
	public void start() {
		System.out.println("PassengerCar started");
	}
}