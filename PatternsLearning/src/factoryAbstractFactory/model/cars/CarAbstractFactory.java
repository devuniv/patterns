package factoryAbstractFactory.model.cars;

//AbstractFactory
//1) might be more complex and have different create methods:
//KingdomAbstractFactory=KingdomFactory with createCastle(), createArmy(), createKing()
//2) name alternative: just CarFactory - from interface type it is clear that it is abstract!
//3) might be abstract class instead of interface. But interface is used more often.
public interface CarAbstractFactory {

	Car create();
}