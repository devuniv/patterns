package factoryAbstractFactory.model.cars;

//ConcreteFactory
//1) can be implemented as a Singleton as only one instance of the ConcreteFactory object is needed
//2) its create() method may take enum PassengerCarType.BMW arg 
//making PassengerCarFactory - a simple factory pattern itself:
// USSRTankFactory.create(USSRTanks.T34), USSRTankFactory.create(USSRTanks.T35), 
//USSRTankFactory implements TankAbstractFactory
public class PassengerCarFactory implements CarAbstractFactory {

	// fields to store values from ctor

	// ctor PassengerCarFactory(arg1, arg2)

	@Override
	public Car create() {

		return new PassengerCar(); // args can be given from fields (which were
									// init thru PassengerCarFactory ctor)
	}
}