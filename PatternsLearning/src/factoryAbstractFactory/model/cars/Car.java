package factoryAbstractFactory.model.cars;

// AbstractProduct (this interface/abstract class is generically called so)
// this could be abstract class (BasicCar/SimpleCar), not interface
//then PassengerCar and FreightCar would extend it
public interface Car {
	void start();
}
