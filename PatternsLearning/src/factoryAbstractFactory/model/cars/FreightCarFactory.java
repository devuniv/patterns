package factoryAbstractFactory.model.cars;

//ConcreteFactory
//can be implemented as a Singleton as only one instance of the ConcreteFactory object is needed
//or max 3 new instances - limited no of: database conn, sockets, open files (file descriptors)
public class FreightCarFactory implements CarAbstractFactory {

	// fields to store values from ctor

	// ctor PassengerCarFactory(arg1, arg2)

	@Override
	public Car create() {

		return new FreightCar(); // args can be given from fields (which were
									// init thru PassengerCarFactory ctor)
	}
}