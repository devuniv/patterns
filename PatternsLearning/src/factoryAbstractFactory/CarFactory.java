package factoryAbstractFactory;

import factoryAbstractFactory.model.cars.Car;
import factoryAbstractFactory.model.cars.CarAbstractFactory;

//ttis class not needed for pattern itself!!!
//it is just helper class for more compact wording of METHOD 2 Example in Driver
class CarFactory {
	public static Car create(CarAbstractFactory factory) {
		return factory.create();
	}
}