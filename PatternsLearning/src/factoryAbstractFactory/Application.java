package factoryAbstractFactory;

import factoryAbstractFactory.model.cars.Car;
import factoryAbstractFactory.model.cars.CarAbstractFactory;
import factoryAbstractFactory.model.cars.FreightCarFactory;
import factoryAbstractFactory.model.cars.PassengerCarFactory;

//this clearly shows usage reason/ideology
class Application {
	// one method (configure()) sets it, other method (runBusinessLogic()) uses
	private CarAbstractFactory carFactory;

	public static void main(String[] args) {

		Application application = new Application();

		// DO IT FIRST: instantiates CarAbstractFactory carFactory field with
		// concrete factory
		application.configure();

		// THEN whole app uses instantiated CarAbstractFactory carFactory
		application.runBusinessLogic();

		// SO WE HAVE ONE POINT TO SWITCH TYPE OF FACTORY - configure() !!!
		// ENTIRE APP JUST USES FACTORY INTERFACE!
	}

	/**
	 * Приложение СНАЧАЛА ОДИН РАЗ создаёт определённую фабрику в зависимости от
	 * конфигурации или окружения.
	 */
	void configure() {
		// if (System.getProperty("os.name").equals("Windows 10"))
		if ("passenger".equals(System.getProperty("car.type"))) {
			carFactory = new PassengerCarFactory();
		} else {
			carFactory = new FreightCarFactory();
		}
	}

	/**
	 * ПОТОМ Весь остальной клиентский код работает с фабрикой и продуктами
	 * только через общий интерфейс, поэтому для него неважно какая фабрика была
	 * создана.
	 */
	void runBusinessLogic() {
		Car car = carFactory.create(); // now we don't care about concrete
										// factory!
		car.start();
	}
}
