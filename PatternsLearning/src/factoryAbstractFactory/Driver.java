package factoryAbstractFactory;

import factoryAbstractFactory.model.cars.Car;
import factoryAbstractFactory.model.cars.CarAbstractFactory;
import factoryAbstractFactory.model.cars.FreightCarFactory;
import factoryAbstractFactory.model.cars.PassengerCarFactory;

// use Factory Method pattern if complexity of AbstractFactory is excessive

//Client
//see also Application class - it clearly shows usage reason/ideology
public class Driver {

	public static void main(String[] args) {

		// METHOD 1
		CarAbstractFactory carFactory = new PassengerCarFactory(); // may give
																	// args
		Car myCar = carFactory.create();
		myCar.start();

		// METHOD 2
		// using helper class CarFactory
		// client don't use CarAbstractFactory variable
		Car myCar1 = CarFactory.create(new PassengerCarFactory());
		myCar1.start();

		// INTERESTING USECASE
		// an array of factory objects of interface type CarAbstractFactory
		CarAbstractFactory[] carFactoryPool = { new PassengerCarFactory(), new FreightCarFactory() };
		// now iterate thru whatever number and whatever concrete factories you
		// then create them all and call their methods, not knowing what kind of
		// cars and how many...
		for (CarAbstractFactory carFactoryFromPool : carFactoryPool) {
			Car car = carFactoryFromPool.create();
			car.start();
		}
	}
}
